#!/bin/bash
# remove old dist directory
rm -rf dist
# build
python -m build
# install locally
pip install --force-reinstall dist/upm*.whl
